__author__ = 'Maciej Cichosz & Piotr Burzyński'
__email__ = 'cichosz.maciej@gmail'
__ver__ = '1.0.0'
__copyright__ = 'Copyright 2020, Maciej Cichosz & Piotr Burzyński'

import pygame as pg
from pygame.math import Vector2
import pygame_textimput as pg_input
import random
import sqlite3 as sq


class Menu:
    def __init__(self):
        self.option_menu = ('START', 'RANK', 'QUIT')
        self.font = pg.font.SysFont('comicsansms', 36)
        self.choice = 0
        self.text = []
        for i in range(len(self.option_menu)):
            self.text.append(self.font.render(self.option_menu[i], True, (255, 255, 255)))

    def render(self, screen):
        for i in range(len(self.option_menu)):
            if i == self.choice:
                self.text[i] = self.font.render(self.option_menu[i], True, (0, 255, 0))
            else:
                self.text[i] = self.font.render(self.option_menu[i], True, (255, 255, 255))
            screen.blit(self.text[i], (575, 250 + i * 50))

    def change_choice(self, change):
        if change == 1:
            if self.choice < 2:
                self.choice += change
        elif change == -1:
            if self.choice > 0:
                self.choice += change

    def get_choice(self) -> int:
        return self.choice


class Ship(pg.sprite.Sprite):

    def __init__(self, pos=(420, 420)):
        super(Ship, self).__init__()
        self.image = pg.image.load('img/ship.png')
        self.original_image = self.image
        self.rect = self.image.get_rect(center=pos)
        self.position = Vector2(pos)
        self.direction = Vector2(1, 0)
        self.speed = 0
        self.speed_down_slow = 10
        self.max_speed = 8
        self.angle_speed = 0
        self.angle = 0
        self.shoot_speed = 5

    def speed_up(self):
        if self.speed < self.max_speed:
            self.speed += 1
        else:
            self.speed = self.max_speed

    def speed_down(self):
        if self.speed_down_slow > 0:
            self.speed_down_slow -= 1
        if self.speed_down_slow == 0:
            self.speed_down_slow = 10
            if self.speed > 0:
                self.speed -= 1

    def if_can_shoot(self):
        if self.shoot_speed == 5:
            self.shoot_speed = 0
            return True

    def get_position(self) -> Vector2:
        return self.position

    def update(self):
        if self.angle_speed != 0:
            self.direction.rotate_ip(self.angle_speed)
            self.angle += self.angle_speed
            self.image = pg.transform.rotate(self.original_image, -self.angle)
            self.rect = self.image.get_rect(center=self.rect.center)
            self.angle_speed = 0

        if self.shoot_speed != 5:
            self.shoot_speed += 1

        self.position += self.direction * self.speed
        self.position.x %= 1300
        self.position.y %= 740
        self.rect.center = self.position


class Missile(pg.sprite.Sprite):
    def __init__(self, pos: Vector2, direction: Vector2):
        super(Missile, self).__init__()
        self.speed = 25
        self.size = 5
        self.position = Vector2(pos)
        self.direction = Vector2(direction)
        self.image = pg.Surface((self.size, self.size), pg.SRCALPHA)
        pg.draw.circle(self.image, (255, 255, 255), (self.size // 2, self.size // 2), self.size // 2)
        self.rect = self.image.get_rect()
        self.rect.center = self.position

    def update(self):
        self.position += self.direction * self.speed
        self.rect.center = self.position
        if self.rect.center[0] > 1300 or self.rect.center[0] < 0 or self.rect.center[1] > 800 or \
                self.rect.center[1] < 0:
            self.kill()


class Rock(pg.sprite.Sprite):
    def __init__(self, player_position: Vector2):
        super(Rock, self).__init__()
        self.speed = random.randint(2, 5)
        self.size = random.randint(50, 90)
        self.__rand_position(player_position)
        self.direction = Vector2((random.uniform(-1, 1), random.uniform(-1, 1)))
        self.image = pg.Surface((self.size, self.size), pg.SRCALPHA)
        pg.draw.circle(self.image, (0, 255, 0), (self.size // 2, self.size // 2), self.size // 2)
        self.rect = self.image.get_rect()

    def __rand_position(self, player_pos: Vector2):
        status = True
        while status:
            rand_vec = Vector2((random.randint(0, 1200), random.randint(0, 800)))
            if player_pos.distance_to(rand_vec) > 200:
                self.position = rand_vec
                status = False

    def update(self):
        self.position += self.direction * self.speed
        self.position.x %= 1300
        self.position.y %= 800
        self.rect.center = self.position


class ScoreBoard:
    def __init__(self):
        self.score = 0
        self.inc_score = 100
        self.font = pg.font.SysFont("comicsansms", 20)
        self.text = self.font.render("Score: " + str(self.score), True, (255, 255, 255))

    def add_score(self):
        self.score += self.inc_score
        self.text = self.font.render("Score: " + str(self.score), True, (255, 255, 255))

    def get_score(self) -> int:
        return self.score

    def render(self, screen):
        screen.blit(self.text, (20, 20))

    def reset(self):
        self.score = 0


class Rank:
    def __init__(self):
        try:
            self.con = sq.connect('rank.db')
            self.cur = self.con.cursor()
        except sq.Error as error:
            print(error)
        finally:
            self.cur.execute("""SELECT name, points FROM  ranks ORDER BY points DESC LIMIT 10;""")
            self.ranks = self.cur.fetchall()
            print(self.ranks)

    def __del__(self):
        self.con.commit()
        self.con.close()

    def __get_score(self):
        self.cur.execute("""SELECT name, points FROM  ranks ORDER BY points DESC LIMIT 10;""")
        self.ranks = self.cur.fetchall()

    def add_score(self, name: str, points: int):
        self.cur.execute("INSERT INTO ranks(name, points) VALUES('" + name + "' , " + str(points) + ")")
        self.cur.fetchall()

    def display_rank(self, screen):
        self.__get_score()
        font = pg.font.SysFont('comicsansms', 20)
        i = 1
        txt = []
        for name, points in self.ranks:
            text = str(i) + '. ' + name + ' ' + str(points)
            txt.append(font.render(text, True, (255, 255, 255)))
            i += 1

        for i in range(len(txt)):
            screen.blit(txt[i], (575, 100 + 50 * i))


def menu(screen, clock) -> int:
    """:return gives status number
        :screen handler to screen
        :clock handler to global clock
    """
    h_menu = Menu()
    run_menu = True
    while run_menu:
        clock.tick(60)
        for event in pg.event.get():
            if event.type == pg.QUIT:
                return 2
                run_menu = False
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_DOWN:
                    h_menu.change_choice(1)
                if event.key == pg.K_UP:
                    h_menu.change_choice(-1)
                if event.key == pg.K_RETURN:
                    return h_menu.get_choice()

        screen.fill((30, 30, 30))
        h_menu.render(screen)
        pg.display.flip()


def ranking(screen, clock):
    run_rank = True
    while run_rank:
        clock.tick(60)
        for event in pg.event.get():
            if event.type == pg.QUIT:
                run_rank = False
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    run_rank = False

        screen.fill((30, 30, 30))
        rank.display_rank(screen)
        pg.display.flip()


def display_add_score(screen, score: int, clock, h_rank):
    text_input = pg_input.TextInput(font_family='comicsansms', text_color=(255, 255, 255), cursor_color=(230, 230, 230))
    font = pg.font.SysFont('comicsansms', 35)
    text1 = 'Your score: ' + str(score)
    text1 = font.render(text1, True, (255, 255, 255))
    text2 = 'Type your name: '
    text2 = font.render(text2, True, (255, 255, 255))
    done = False
    while not done:
        clock.tick(30)
        screen.fill((30, 30, 30))
        events = pg.event.get()
        for event in events:
            if event.type == pg.QUIT:
                exit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_RETURN:
                    h_rank.add_score(text_input.get_text(), score)
                    done = True
                if event.key == pg.K_ESCAPE:
                    done = True

        text_input.update(events)
        screen.blit(text1, (500, 300))
        screen.blit(text2, (420, 350))
        screen.blit(text_input.get_surface(), (690, 350))
        pg.display.update()


def main() -> int:
    """
    :return: gives status about player's death
    """
    pg.init()
    screen = pg.display.set_mode((1280, 720))
    score = ScoreBoard()
    player = Ship((420, 420))
    max_num_rocks = 10
    done = False
    missile_sprites = pg.sprite.Group()
    rock_sprites = pg.sprite.Group()
    player_sprite = pg.sprite.RenderPlain(player)
    clock = pg.time.Clock()

    game_status = menu(screen, clock)
    if game_status == 2:
        pg.quit()
        return 0
    elif game_status == 1:
        ranking(screen, clock)
        return 1
    elif game_status == 0:
        done = False

    while not done:
        clock.tick(60)
        for event in pg.event.get():
            if event.type == pg.QUIT:
                return 0
        keys = pg.key.get_pressed()
        if keys[pg.K_UP]:
            player.speed_up()

        if keys[pg.K_UP] == 0:
            player.speed_down()

        if keys[pg.K_LEFT]:
            player.angle_speed = -6

        if keys[pg.K_RIGHT]:
            player.angle_speed = 6

        if keys[pg.K_SPACE]:
            if player.if_can_shoot():
                missile_sprites.add(Missile(player.position, player.direction))

        if len(rock_sprites) < max_num_rocks:
            rock_sprites.add(Rock(player.get_position()))

        if pg.sprite.groupcollide(missile_sprites, rock_sprites, 1, 1):
            score.add_score()

        if pg.sprite.groupcollide(rock_sprites, player_sprite, 0, 0):
            display_add_score(screen, score.get_score(), clock, rank)
            return 1

        player_sprite.update()
        missile_sprites.update()
        rock_sprites.update()
        screen.fill((30, 30, 30))
        player_sprite.draw(screen)
        missile_sprites.draw(screen)
        rock_sprites.draw(screen)
        score.render(screen)
        pg.display.flip()


if __name__ == '__main__':
    s = True
    rank = Rank()
    while s:
        if main() == 0:
            s = False

    pg.quit()
